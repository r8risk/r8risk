from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('insurance/', views.insurance, name='insurance'),
    path('investments/', views.investments, name='investments'),
    path('technology/', views.technology, name='technology'),
    path('world/', views.world, name='world'),
    path('cannabis/', views.cannabis, name='cannabis'),
    path('blockchain/', views.blockchain, name='blockchain'),
    path('blog/', views.blog, name='blog'),
    path('welcome/', views.welcome, name='welcome'),
    path('principlesandculture/', views.principlesandculture, name='principlesandculture'),
    path('whatwedo/', views.whatwedo, name='whatwedo'),
    path('ourleadership/', views.ourleadership, name='ourleadership'),
    path('community/', views.community, name='community'),
    path('contact/', views.contact, name='contact'),
    path('blockchain/', views.blockchain, name='blockchain'),
]
