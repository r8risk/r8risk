from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.

def home(request):
    return render(request, 'home.html', {})

def insurance(request):
    return render(request, 'insurance.html')

def investments(request):
    return render(request, 'investments.html')

def technology(request):
    return render(request, 'technology.html')

def world(request):
    return render(request, 'world.html')

def cannabis(request):
    return render(request, 'cannabis.html')

def blockchain(request):
    return render(request, 'blockchain.html')

def blog(request):
    return render(request, 'blog.html')

def welcome(request):
    return render(request, 'welcome.html')

def principlesandculture(request):
    return render(request, 'principlesandculture.html')

def whatwedo(request):
    return render(request, 'whatwedo.html')

def ourleadership(request):
    return render(request, 'ourleadership.html')

def community(request):
    return render(request, 'community.html')

def contact(request):
    return render(request, 'contact.html')
